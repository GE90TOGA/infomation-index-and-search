all: a3search

a3search: a3search.o stmr.o
	g++-4.9 -o a3search a3search.o stmr.o

stmr.o: stmr.c stmr.h
	gcc-4.9 -O3 -c stmr.c

a3search.o: a3search.cpp stmr.h
	g++-4.9 -std=c++14 -Wall -Werror -O3 -c a3search.cpp

clean:
	rm *o a3search