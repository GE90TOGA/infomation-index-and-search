## COMP9319 Assignment 3
written by Li Quan z5044754

### Stemming library

The program uses a C stemming library called 'stmr' which can be accessed from 'https://github.com/wooorm/stmr.c'.

### Design of index files

- The program will scan each file line by line. Parsing text and filter only English word only which only contains a-z
and A-Z with minimum length of 3.

- The index is built by a simple format which is '<word> <fileId> <fileName>' line by line.

- After processing a certain amount of words, the program will write the index it built to file, which frees
its memory.

- After finishing writing all index files. The program merges all index to a single file by doing a few 2 way merges.

### Design of search

- The index file is built to have a ascending lexicographic order. For a given set of search terms. It first sort
and stem them, then read index file line by line. If the word from index matches the word in term, it does 2 updating.
(1) insert the fileId to the the a set. (2) insert the filed id and count to a map. Some optimisation tricks are used
such as comparing the last search term with the word read from index. If the word from index is lexically after the the
last term, then stop the search and output no match.

- After searching, the program intersect all set which resulting a final set that contains all matching files. Then it
goes to the map, count the total frequency for each file.

- At last, the program sort the files based on its frequency and if same frequency appears, compare by file name.