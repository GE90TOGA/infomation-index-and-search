#include <iostream>
#include <string.h>
#include <dirent.h>
#include <vector>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <regex>
#include <map>
#include <list>
#include <chrono>
#include <set>


/*include the external stemming library in this way because it is written in C*/
extern "C" {
#include "stmr.h"
}

typedef std::pair<std::string, int> PAIR;

class A3Search {
public:
    A3Search(const std::string &searchPath, const std::string &indexPath) : searchPath_(searchPath),
                                                                            indexPath_(indexPath),
                                                                            stopWordSet_(
                                                                                    {"a", "about", "above", "above",
                                                                                     "across", "after", "afterwards",
                                                                                     "again", "all", "almost",
                                                                                     "alone", "along", "already",
                                                                                     "also", "although",
                                                                                     "always", "am", "among", "amongst",
                                                                                     "amount", "an", "and",
                                                                                     "another",
                                                                                     "any", "anyhow", "anyone",
                                                                                     "anything", "anyway", "anywhere",
                                                                                     "are", "around",
                                                                                     "as", "at", "back", "be", "became",
                                                                                     "because", "become", "becomes",
                                                                                     "becoming",
                                                                                     "been", "before", "beforehand",
                                                                                     "behind", "being", "below",
                                                                                     "beside", "besides",
                                                                                     "between", "beyond",
                                                                                     "both", "bottom", "but", "by",
                                                                                     "call", "can",
                                                                                     "cannot", "cant", "co", "con",
                                                                                     "could", "couldnt", "cry", "de",
                                                                                     "detail", "do", "done", "down",
                                                                                     "due", "during", "each", "eg",
                                                                                     "eight", "either",
                                                                                     "eleven", "else", "elsewhere",
                                                                                     "empty", "enough", "etc", "even",
                                                                                     "ever", "every",
                                                                                     "everyone", "everything",
                                                                                     "everywhere", "except", "few",
                                                                                     "had",
                                                                                     "has", "hasnt", "have", "he",
                                                                                     "hence", "her", "here",
                                                                                     "hereafter", "hereby",
                                                                                     "herein", "hereupon", "hers",
                                                                                     "herself", "him", "himself", "his",
                                                                                     "how",
                                                                                     "however", "hundred", "ie", "if",
                                                                                     "in", "inc", "indeed",
                                                                                     "into", "is",
                                                                                     "it", "its", "itself", "keep",
                                                                                     "last", "latter", "latterly",
                                                                                     "least", "less",
                                                                                     "ltd", "made", "many", "may", "me",
                                                                                     "meanwhile", "might", "mill",
                                                                                     "mine", "more",
                                                                                     "moreover", "most", "mostly",
                                                                                     "move", "much", "my",
                                                                                     "myself", "name",
                                                                                     "namely", "neither", "never",
                                                                                     "nevertheless", "next", "nine",
                                                                                     "no", "nobody",
                                                                                     "none", "noone", "nor", "not",
                                                                                     "nothing", "now", "nowhere", "of",
                                                                                     "off", "often",
                                                                                     "on", "once", "one", "only",
                                                                                     "onto", "or", "other", "others",
                                                                                     "otherwise", "our",
                                                                                     "ours", "ourselves", "out", "over",
                                                                                     "own", "part", "per", "perhaps",
                                                                                     "please",
                                                                                     "put", "rather", "re", "same",
                                                                                     "see", "seem", "seemed", "seeming",
                                                                                     "seems",
                                                                                     "several", "she",
                                                                                     "show", "side", "since",
                                                                                     "six",
                                                                                     "so", "some", "somehow",
                                                                                     "someone", "something", "sometime",
                                                                                     "somewhere", "still", "such",
                                                                                     "take", "ten", "than",
                                                                                     "that", "the",
                                                                                     "their", "them", "themselves",
                                                                                     "then", "thence", "there",
                                                                                     "thereafter", "thereby",
                                                                                     "therefore", "therein",
                                                                                     "these", "they",
                                                                                     "thin", "third",
                                                                                     "this", "those", "though", "three",
                                                                                     "through", "throughout", "thru",
                                                                                     "thus", "to",
                                                                                     "together", "too", "top", "toward",
                                                                                     "towards", "twelve", "twenty",
                                                                                     "two", "un",
                                                                                     "under", "until", "up", "upon",
                                                                                     "us", "very", "via", "was", "we",
                                                                                     "well", "were",
                                                                                     "what", "whatever", "when",
                                                                                     "whence", "whenever", "where",
                                                                                     "whereafter",
                                                                                     "whereas", "whereby", "wherein",
                                                                                     "whereupon", "wherever", "whether",
                                                                                     "which",
                                                                                     "while", "whither", "who",
                                                                                     "whoever", "whole", "whom",
                                                                                     "whose", "why", "will",
                                                                                     "with", "within", "without",
                                                                                     "would", "yet", "you", "your",
                                                                                     "yours", "yourself",
                                                                                     "yourselves", "the"}) {
        // create index dir if it does not exists
        if (!isIndexDirExsits()) {
            // std::cout<<"create new index dir"<<std::endl;
            int returnCode = system(("mkdir " + indexPath_).c_str());
            if (returnCode != 0) {
                return;
            }
        }

        if (existFile(indexPath + "/" + MERGED_FILE)) {
            readfileIdMap(); // read the stored filename -> id mapping
        } else {
            // if index is never build, do the following
            matcher_.assign("[a-zA-Z]{3,}"); // compile the regex
            getFileList(searchPath, files_, false); // read all non hidden file under that directory
            generateFileIdMapping(); // generate the filename -> id mapping
            buildIndex(); // build index for text files
            freePostingMap(); // free the last part of posting map
            mergeIndex(); // merge possible split index files to one single file
        }
    }

    /**
     * The core searching function, search words from index file
     * @param sortedSearchTerms a vector of string which contains the sorted search term
     */
    void search(const std::vector<std::string> sortedSearchTerms) {
        std::ifstream indexInStream(indexPath_ + "/" + MERGED_FILE);
        std::string line = "";
        std::string lastTerm = sortedSearchTerms.back();
        int termCount = static_cast<int>(sortedSearchTerms.size());
        int termIndex = 0;
        std::unordered_set<int> termFileSets[termCount];
        // the array of set which contains the matching file id for nth search term
        while (getline(indexInStream, line)) {
            if (line == "") {
                break;
            }
            std::string curWord = getWord(line); // read a word from index file
            if (lastTerm < curWord) { // if the word is lexically greater than the last term, can stop
                break;
            }
            if (termIndex < termCount) {
                if (sortedSearchTerms.at(termIndex) == curWord) {
                    processIndexLine(line, termFileSets, termIndex);
                    ++termIndex; // move to next term
                }
            }
        }
        indexInStream.close();
        // intersect multiple term common file Ids
        std::unordered_set<int> mergeSet;
        if (termCount > 1) { // intersect all result set
            // intersectTwoSets(termFileSets[0],termFileSets[1]);
            mergeSet = intersectTwoSets(termFileSets[0], termFileSets[1]);
            if (mergeSet.size() == 0) { // not find anything
                std::cout << std::endl;
                return;
            }
            for (int i = 2; i < termCount; ++i) {
                mergeSet = intersectTwoSets(termFileSets[i], mergeSet);
                if (mergeSet.size() == 0) { // not find anything
                    std::cout << std::endl;
                    return;
                }
            }
        } else {
            mergeSet = termFileSets[0];
        }

        if (mergeSet.size() == 0) {
            std::cout << std::endl;
            return;
        }

        // there are matches
        // printPostingSearch();
        // count the frequency for each file
        countKeywordMatchBySet(sortedSearchTerms, mergeSet);
        // put file name in the vector
        std::vector<PAIR> finalRankingVector(fileNameAndMatchCountMap_.cbegin(),
                                             fileNameAndMatchCountMap_.cend());
        std::sort(finalRankingVector.begin(), finalRankingVector.end(),
                  [](const PAIR &x, const PAIR &y) {
                      if (x.second > y.second) {
                          return true;
                      } else if (x.second < y.second) {
                          return false;
                      } else {
                          return x.first < y.first;
                      }
                  });
        for (auto item : finalRankingVector) {
            std::cout << item.first;
            // std::cout << " " << item.second;
            std::cout << std::endl;
        }
    }

    /**
     * Call the stmr.c library to stem the word
     * @param original word
     * @return string of stemed word
     */
    std::string stemWord(const std::string &original) {
        char *word = new char[original.length() + 1];
        strcpy(word, original.c_str());
        int end = stem(word, 0, strlen(word) - 1);
        word[end + 1] = 0;
        std::string afterStemStr(word);
        delete[] word;
        return afterStemStr;
    }
    // convert a string to lower case
    void toLowerCase(std::string &s) {
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    }


private:
    const std::string ID_MAP_NAME = "fileIdMap.dat";
    const std::string MERGING_FILE_NAME = "merging";
    const std::string MERGED_FILE = "merged";
    const bool BLOCK_SPLITION = true;
    const int MAX_WORD_ALLOWED = 800000; // the threshold to trigger index file splition
    std::string searchPath_;
    std::string indexPath_;
    std::set<std::string> stopWordSet_;
    std::vector<std::string> files_;
    std::unordered_map<int, std::string> fileIdMaper_;
    std::vector<std::string> filelinevec_;
    std::map<std::string, std::unordered_map<int, int>> *postingMap_;
    std::map<std::string, std::unordered_map<int, int>> postingSearchMap_;
    std::unordered_map<int, int> fileMatchCountMap_;
    std::unordered_map<std::string, int> fileNameAndMatchCountMap_;
    bool postingNewed_ = false;
    int indexBlockCounter = 0;
    int wordCounter_ = 0;
    std::regex matcher_;
    // find the index dir
    bool isIndexDirExsits() {
        DIR *dir = opendir(indexPath_.c_str());
        if (dir) {
            /* Directory exists. */
            closedir(dir);
            return true;
        } else if (ENOENT == errno) {
            return false;
        }
        return false;
    }
    // read the fileid -> map from file
    void readfileIdMap() {
        std::ifstream idmapInStream(indexPath_ + "/" + ID_MAP_NAME);
        std::string line = "";
        while (getline(idmapInStream, line)) {
            if (line != "") {
                std::string fileName = getWord(line);
                int id = std::stoi(getFileFreqs(line));
                fileIdMaper_.insert({id, fileName});
            }
        }
        idmapInStream.close();
    }
    // check if file exists
    bool existFile(const std::string &fName) {
        return std::ifstream(fName) ? true : false;
    }

    /**
     * count the total freq each search term
     * @param searchTerms
     * @param fileSet
     */
    void countKeywordMatchBySet(const std::vector<std::string> searchTerms,
                                const std::unordered_set<int> &fileSet) {
        for (auto term : searchTerms) {
            for (auto fileId: fileSet) {
                int freq = getTermFileCount(term, fileId);
                addFileCountToMap(fileId, freq);
            }
        }
        for (auto idmatch_itr = fileMatchCountMap_.cbegin();
             idmatch_itr != fileMatchCountMap_.end(); ++idmatch_itr) {
            std::string fileName = getFileNameById(idmatch_itr->first);
            int freq = idmatch_itr->second;
            fileNameAndMatchCountMap_.insert({fileName, freq});
        }

        std::map<std::string, std::unordered_map<int, int>> clearPostingMap;
        std::unordered_map<int, int> clearfilMatchMap;
        std::swap(postingSearchMap_, clearPostingMap);
        std::swap(fileMatchCountMap_, clearfilMatchMap);
    }

    /**
     * add file count id into the map when doing the search
     * @param fileId
     * @param freq
     */
    void addFileCountToMap(const int fileId, const int freq) {
        auto fileCount_it = fileMatchCountMap_.find(fileId);
        if (fileCount_it == fileMatchCountMap_.end()) {
            fileMatchCountMap_.insert({fileId, freq});
        } else {
            int prevFreq = fileCount_it->second;
            int newFreq = freq + prevFreq;
            fileMatchCountMap_.erase(fileCount_it);
            fileMatchCountMap_.insert({fileId, newFreq});
        }
    }

    /**
     * read the index file and get the right (freq count) part
     * @param word
     * @param fileId
     * @return
     */
    int getTermFileCount(const std::string &word, const int fileId) {
        auto word_it = postingSearchMap_.find(word);
        auto file_it = (word_it->second).find(fileId);
        return file_it->second;
    }

    /**
     * check if it is stop word
     * @param word
     * @return
     */
    bool isStopWord(const std::string &word) {
        auto it = stopWordSet_.find(word);
        return it == stopWordSet_.end() ? false : true;
    }

    /**
     * when index word matches search term, record it in map and set.
     * @param line
     * @param termFileSets
     * @param termIndex
     */
    void processIndexLine(const std::string &line, std::unordered_set<int> *termFileSets, int termIndex) {
        // [termCount];
        using namespace std;
        vector<string> lineVec = splitToVec(line, ' ');
        string word = lineVec[0];
        for (unsigned int index = 1; index < lineVec.size(); ++index) {
            if (index % 2 == 0) {
                int fileId = std::stoi(lineVec[index - 1]);
                int freq = std::stoi(lineVec[index]);
                insertToPostingSearchMap(word, fileId, freq);
                termFileSets[termIndex].insert(fileId);
            }
        }
        return;
    }

    /**
     * insert the word to the search map during index building
     * @param word
     * @param fileId
     * @param freq
     */
    void insertToPostingSearchMap(const std::string &word, int fileId, int freq) {
        using namespace std;
        auto posting_search_map_iter = postingSearchMap_.find(word);
        if (posting_search_map_iter == postingSearchMap_.end()) { // not find
            unordered_map<int, int> fileFreq;
            fileFreq.insert({fileId, freq});
            postingSearchMap_.insert({word, fileFreq});
        } else {
            (posting_search_map_iter->second).insert({fileId, freq});
        }
    }


    /**
     * Read a file names under a dir except for hidden file
     * @param dir
     * @param files
     * @param isIndex if reading index file, also omit the (file id map) file
     * @return
     */
    int getFileList(const std::string &dir, std::vector<std::string> &files, bool isIndex) {
        DIR *dp;
        struct dirent *dirp;
        dp = opendir(dir.c_str());
        while ((dirp = readdir(dp)) != NULL) {
            std::string filename(dirp->d_name);
            if (filename[0] != '.') {
                if (isIndex && filename == ID_MAP_NAME) {
                    continue;
                }
                files.push_back(std::string(dirp->d_name));
            }
        }
        closedir(dp);
        return 0;
    }

    /**
     * Generate file id mapping
     */
    void generateFileIdMapping() {
        std::string fileMappingFilePath = indexPath_ + "/" + ID_MAP_NAME;
        std::ofstream fileMappingFile(fileMappingFilePath);
        int id = 0;
        for (auto fileName : files_) {
            fileMappingFile << fileName << " " << id << std::endl;
            fileIdMaper_.insert({id, fileName});
            ++id;
        }
        files_.clear();
        fileMappingFile.close();
    }

    /**
     * Get file name by file id
     * @param id file id
     * @return
     */
    std::string getFileNameById(const int id) {
        auto got = fileIdMaper_.find(id);
        if (got == fileIdMaper_.end()) {
            return "";
        }
        return got->second;
    }

    /**
     * parse a string line to vector by regex which only retains English words with minimum length 3
     * @param line
     */
    void parseLineToVec(const std::string &line) {
        using namespace std;
        for (sregex_iterator it(line.begin(), line.end(), matcher_), end;
             it != end;
             ++it) {
            string word = it->str();
            toLowerCase(word);
            if (!isStopWord(word)) {
                filelinevec_.push_back(word);
            }
        }
    }

    /**
     * Builiding the index for files
     * @return
     */
    int buildIndex() {
        int id = 0;
        std::ifstream originalFile;
        createPostingMap();
        for (auto it : fileIdMaper_) {
            if (originalFile.is_open()) {
                originalFile.close();
            }
            std::string fileName = getFileNameById(id);
            originalFile.open(searchPath_ + "/" + fileName);
            buildIndexForFile(originalFile, id);
            if (wordCounter_ >= MAX_WORD_ALLOWED && BLOCK_SPLITION) {
                createIndexBlock();
            }
            ++id;
        }
        createIndexBlock(); // create index for the last remaining block
        if (originalFile.is_open()) {
            originalFile.close();
        }
        return 0;
    }

    // safe way to allocate memory for index
    int createPostingMap() {
        if (!postingNewed_) {
            postingMap_ = new std::map<std::string, std::unordered_map<int, int>>;
            postingNewed_ = true;
        }
        return 0;
    }

    // safely frees the memory allocated for index map
    int freePostingMap() {
        if (postingNewed_) {
            delete postingMap_;
            postingNewed_ = false;
        }
        return 0;
    }

    // print the posting (debug)
    void printPosting() {
        for (auto it = (*postingMap_).begin(); it != (*postingMap_).end(); ++it) {
            std::cout << it->first << std::endl;
            for (auto fileFreq : it->second) {
                std::cout << "\t" << fileFreq.first << " " << fileFreq.second << std::endl;
            }
        }
    }
    // debug function for creating posintg
    void printPostingSearch() {
        for (auto it = postingSearchMap_.begin(); it != postingSearchMap_.end(); ++it) {
            std::cout << it->first << std::endl;
            for (auto fileFreq : it->second) {
                std::cout << "\t" << fileFreq.first << " " << fileFreq.second << std::endl;
            }
        }
    }

    /**
     * build index for a given file
     * @param originalFile
     * @param fileId
     */
    void buildIndexForFile(std::ifstream &originalFile, int fileId) {
        using namespace std;
        string line = "";
        while (std::getline(originalFile, line)) {
            parseLineToVec(line);
            for (auto s: filelinevec_) {
                string afterStemStr = stemWord(s);
                if (afterStemStr.length() > 0) {
                    insertWordToPostingMap(afterStemStr, fileId);
                }
            }
            filelinevec_.clear();
        }

    }
    // write a block of index to file
    int writeIndexToFile(const int indexBlockId) {
        if ((*postingMap_).size() == 0) {
            return 0;
        }
        std::ofstream indexFileStream;
        std::string indexIdstr = std::to_string(indexBlockId);
        indexFileStream.open(indexPath_ + '/' + indexIdstr);
        for (auto it = (*postingMap_).begin(); it != (*postingMap_).end(); ++it) {
            indexFileStream << it->first;
            for (auto fileFreq_itr : it->second) {
                indexFileStream << " " << fileFreq_itr.first << " " << fileFreq_itr.second;
            }
            indexFileStream << std::endl;
        }
        indexFileStream.close();
        return 0;
    }

    /**
     * Put the stemmed word into a hash map which contains the 2 D maping of word->file->count
     * @param stemedWord
     * @param fileId
     * @return
     */
    int insertWordToPostingMap(const std::string stemedWord, int fileId) {
        using namespace std;
        ++wordCounter_;
        auto postingMap_iterator = (*postingMap_).find(stemedWord);
        if (postingMap_iterator == (*postingMap_).end()) {
            unordered_map<int, int> fileFreq;
            fileFreq.insert({fileId, 1});
            (*postingMap_).insert({stemedWord, fileFreq});
        } else {
            auto fileFreq_iterator = (postingMap_iterator->second).find(fileId);
            if (fileFreq_iterator == postingMap_iterator->second.end()) {
                (postingMap_iterator->second).insert({fileId, 1});
            } else {
                int freq = (fileFreq_iterator->second) + 1;
                (postingMap_iterator->second).erase(fileFreq_iterator);
                (postingMap_iterator->second).insert({fileId, freq});
            }
        }
        return 0;
    }

    /**
     * write a block of index to file
     * @return
     */
    int createIndexBlock() {
        writeIndexToFile(indexBlockCounter);
        freePostingMap();
        createPostingMap();
        ++indexBlockCounter;
        wordCounter_ = 0;
        return 0;
    }

    /**
     * Merge all index files to a single one
     * @return
     */
    int mergeIndex() {
        using namespace std;
        vector<string> indexFiles;
        getFileList(indexPath_, indexFiles, true);
        int totalIndexCount = indexFiles.size();
        if (totalIndexCount > 1) {
            std::string mergedName = mergeTwoFile(indexFiles.at(0), indexFiles.at(1));
            for (int index = 2; index < totalIndexCount; ++index) {
                mergedName = mergeTwoFile(mergedName, indexFiles.at(index));
            }
        } else {
            rename((indexPath_ + "/" + "0").c_str(), (indexPath_ + "/" + MERGED_FILE).c_str());
        }
        return 0;
    }

    /**
     * Merge two index files into a single file
     * delete the two file after merge and return the merged new file name
     * @param fileOne
     * @param fileTwo
     * @return
     */
    std::string mergeTwoFile(const std::string &fileOne, const std::string &fileTwo) {
        std::ifstream leftFileStream(indexPath_ + "/" + fileOne);
        std::ifstream rightFileStream(indexPath_ + "/" + fileTwo);
        std::ofstream afterMergeStream(indexPath_ + "/" + MERGING_FILE_NAME);
        std::string lineLeft = "";
        std::string lineRight = "";
        getline(leftFileStream, lineLeft);
        getline(rightFileStream, lineRight);
        bool leftExit = false;
        while (true) {
            if (lineLeft == "") {
                leftExit = true;
                break;
            }
            if (lineRight == "") {
                break;
            }
            std::string s1 = getWord(lineLeft);
            std::string s2 = getWord(lineRight);
            if (s1 < s2) {
                afterMergeStream << lineLeft << std::endl;
                lineLeft = "";
                getline(leftFileStream, lineLeft);
            } else if (s1 > s2) {
                afterMergeStream << lineRight << std::endl;
                lineRight = "";
                getline(rightFileStream, lineRight);
            } else {
                std::string fileFreqAppender = getFileFreqs(lineRight);
                afterMergeStream << lineLeft << fileFreqAppender << std::endl;
                lineLeft = "";
                lineRight = "";
                getline(leftFileStream, lineLeft);
                getline(rightFileStream, lineRight);

            }
        }
        if (leftExit) {
            afterMergeStream << lineRight << std::endl;
            singleFinish(rightFileStream, afterMergeStream);
        } else {
            afterMergeStream << lineLeft << std::endl;
            singleFinish(leftFileStream, afterMergeStream);
        }
        afterMergeStream.close();
        rightFileStream.close();
        leftFileStream.close();

        remove((indexPath_ + "/" + fileOne).c_str());
        remove((indexPath_ + "/" + fileTwo).c_str());
        rename((indexPath_ + "/" + MERGING_FILE_NAME).c_str(), (indexPath_ + "/" + MERGED_FILE).c_str());
        return MERGED_FILE;
    }

    /**
     * process the remaining part of index file during 2 way merge
     * @param indexFileStream
     * @param afterMergeStream
     */
    void singleFinish(std::ifstream &indexFileStream, std::ofstream &afterMergeStream) {
        std::string line = "";
        while (getline(indexFileStream, line)) {
            if (line != "") {
                afterMergeStream << line;
            }
            afterMergeStream << std::endl;
        }
    }

    // split string by a char delim
    template<typename Out>
    void split(const std::string &s, char delim, Out result) {
        std::stringstream ss;
        ss.str(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            *(result++) = item;
        }
    }

    // split string to a vector of string
    std::vector<std::string> splitToVec(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, std::back_inserter(elems));
        return elems;
    }

    // get the word sotred in index line
    std::string getWord(const std::string &line) {
        int firstSpace = (int) line.find(" ");
        return line.substr(0, firstSpace);
    }
    // get the file freq from a index line
    std::string getFileFreqs(const std::string &line) {
        int firstSpace = (int) line.find(" ");
        return line.substr(firstSpace, line.size());
    }

    template<typename IterIn1, typename IterIn2, typename OutputIter>
    OutputIter __intersect__set(IterIn1 b1, IterIn1 e1, IterIn2 b2, IterIn2 e2, OutputIter out) {
        while (!(b1 == e1)) {
            if (!(std::find(b2, e2, *b1) == e2)) {
                *out = *b1;
                ++out;
            }
            ++b1;
        }
        return out;
    }

    /**
     * intersect two sets and return the intersected set
     * @param set1
     * @param set2
     * @param resultSet
     */
    std::unordered_set<int> intersectTwoSets(const std::unordered_set<int> &set1,
                                             const std::unordered_set<int> &set2) {
        std::unordered_set<int> mergedSet;
        __intersect__set(set1.cbegin(), set1.cend(),
                         set2.cbegin(), set2.cend(),
                         std::inserter(mergedSet, mergedSet.begin()));
        return mergedSet;
    }
};


int main(int argc, char **argv) {
    using namespace std;
    std::string filePath(argv[1]);
    std::string indexPath(argv[2]);
    A3Search a3Search(filePath, indexPath);
    vector<string> searchTerms;
    string term1(argv[3]);
    if (term1 != "-c") {
        for (int i = 3; i < argc; ++i) {
            std::string term(argv[i]);
            a3Search.toLowerCase(term);
            term = a3Search.stemWord(term); //stem
            searchTerms.push_back(term);
        }
    }else{
        for (int i = 5; i < argc; ++i) {
            std::string term(argv[i]);
            a3Search.toLowerCase(term);
            term = a3Search.stemWord(term); //stem
            searchTerms.push_back(term);
        }
    }
    std::sort(searchTerms.begin(), searchTerms.end());
    a3Search.search(searchTerms);
    return 0;
}
